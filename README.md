# ecp-cookie-init
Utility for obtaining short-lived cookies for accessing Shibbolized SPs from
command-line tools (e.g., curl or git)

[![pipeline status](https://git.ligo.org/computing/iam/ecp-cookie-init/badges/main/pipeline.svg)](https://git.ligo.org/computing/iam/ecp-cookie-init/commits/main)
[![coverage](https://git.ligo.org/computing/iam/ecp-cookie-init/badges/main/coverage.svg)](https://git.ligo.org/computing/iam/ecp-cookie-init/commits/main)
