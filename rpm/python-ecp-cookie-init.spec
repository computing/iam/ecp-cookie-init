%global srcname ecp-cookie-init
%global distname %{lua:name = string.gsub(rpm.expand("%{srcname}"), "[.-]", "_"); print(name)}
%global version 2.0.1
%global release 1

Name:    python-%{srcname}
Version: %{version}
Release: %{release}%{?dist}
Summary: Utility for obtaining short-lived cookies for accessing Shibbolized SPs

Source0: %pypi_source %distname

License: GPLv3+
Url: https://git.ligo.org/computing/iam/ecp-cookie-init

BuildArch: noarch
Prefix: %{_prefix}

# -- build requirements

BuildRequires: python3-devel
BuildRequires: python3dist(argparse-manpage)
BuildRequires: python3dist(ciecplib)
BuildRequires: python3dist(pip)
BuildRequires: python3dist(pytest)
BuildRequires: python3dist(setuptools)
BuildRequires: python3dist(wheel)

# -- packages

%global _description %{expand:
Utility for obtaining short-lived cookies for accessing Shibbolized SPs.
}

%description %_description

# python3-ecp-cookie-init
%package -n python3-%{srcname}
Summary: Python %{python3_version} modules for %{srcname}
%description -n python3-%{srcname}
%_description
This package provides the %{python3_version} library.
%files -n python3-%{srcname}
%doc README.md
%license COPYING
%{python3_sitelib}

# ecp-cookie-init
%package -n %{srcname}
Summary: Command-line interfaces for %{srcname}
Requires: python3-%{srcname} = %{version}-%{release}
%description -n %{srcname}
%_description
This package provides the command-line interface scripts.
%files -n %{srcname}
%license COPYING
%doc README.md
%{_bindir}/*
%{_mandir}/man1/*.1*

# -- build

%prep
%autosetup -n %{distname}-%{version}

%define setuptools_version %(%python3 -c "import setuptools; print(setuptools.__version__.split('.', 1)[0])")
%if 0%{?setuptools_version} < 61
echo "Writing setup.cfg for setuptools %{setuptools_version}"
# hack together setup.cfg for old setuptools to parse
cat > setup.cfg << SETUP_CFG
[metadata]
name = %{srcname}
version = %{version}
description = %{summary}
license = %{license}
license_files = LICENSE
url = %{url}
[options]
packages = find:
python_requires = >=3.6
install_requires =
  ciecplib
[options.entry_points]
console_scripts =
  ecp-cookie-init = ecp_cookie_init:ecp_cookie_init
[build_manpages]
manpages =
  man/ecp-cookie-init.1:function=create_parser:module=ecp_cookie_init
SETUP_CFG
%endif

%if %{undefined pyproject_wheel}
echo "Writing setup.py for py3_build_wheel"
# write a setup.py to be called explicitly
cat > setup.py << SETUP_PY
from setuptools import setup
setup()
SETUP_PY
%endif

%build
# build a wheel
%if %{defined pyproject_wheel}
%pyproject_wheel
%else
%py3_build_wheel
%endif
# generate manuals
%python3 -c "from setuptools import setup; setup()" \
  --command-packages=build_manpages \
  build_manpages \
;

%install
# install the wheel
%if %{defined pyproject_wheel}
%pyproject_install
%else
%py3_install_wheel %{distname}-%{version}-*.whl
%endif
# install the manuals
%__mkdir -p -v %{buildroot}%{_mandir}/man1
%__install -m 644 -p -v man/*.1* %{buildroot}%{_mandir}/man1/

%check
export PATH="%{buildroot}%{_bindir}:${PATH}"
export PYTHONPATH="%{buildroot}%{python3_sitelib}"
# run the unit tests
%pytest --pyargs ecp_cookie_init.tests
# test basic executable functionality
ecp-cookie-init -h
ecp-cookie-init -v

# -- changelog

%changelog
* Thu Jun 17 2021 Duncan Macleod <duncan.macleod@ligo.org> - 2.0.1-1
- update to 2.0.1

* Thu Jan 28 2021 Duncan Macleod <duncan.macleod@ligo.org> - 2.0.0-1
- rewrite in python

* Thu Dec 03 2020 Satya Mohapatra <patra@mit.edu> - 1.3.7-1
- kagra access
* Wed Dec 21 2016 Paul Hopkins <paul.hopkins@ligo.org> - 1.3.6-1
- Add option to force Kerberos authentication by skipping klist check
- Allow target output to be redirected to a file, or not output at all
- Add options for quiet mode, and to specify cookiefile
* Mon Jun 13 2016 Paul Hopkins <paul.hopkins@ligo.org> - 1.3.5-1
- Use system version of Python
- Fixed for Python 3
* Wed Nov 04 2015 Paul Hopkins <paul.hopkins@ligo.org> - 1.3.4-1
- Use system version of curl
- Fixed bugs in Kerberos support
* Thu Oct 15 2015 Paul Hopkins <paul.hopkins@ligo.org> - 1.3.3-1
- Added explicit Kerberos support
- Added concurrent cookie sessions
- Added invalid password warning
- Added destroy option
* Thu Aug 27 2015 Paul Hopkins <paul.hopkins@ligo.org> - 1.3.2-1
- Fixed ecp-cookie-init reporting incorrect version
* Mon Jul 20 2015 Paul Hopkins <paul.hopkins@ligo.org> - 1.3.1-1
- Added automatic failover for LIGO IdP servers
* Wed Apr 03 2013 Peter Couvares <peter.couvares@ligo.org> - 1.1.0-1
- Fixed for MacOS.
- Added LIGO Guest and Cardiff University IdP support.
- Fixed typo in error message.
* Thu Mar 14 2013 Peter Couvares <peter.couvares@ligo.org> - 1.0.0-1
- Initial version.
